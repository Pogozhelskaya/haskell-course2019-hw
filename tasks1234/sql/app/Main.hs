module Main where

import Text.Megaparsec
import Text.Pretty.Simple(pPrint)

import Lib
import AST
import Parse
import Interpreter

main :: IO ()
main = do
    pPrint $ parseMaybe statementList ("CREATE TABLE qwerty (id int, name varchar(255));"
                                    ++ "SELECT * FROM qwerty WHERE id <> 2 AND id < 6 OR id > 6")
    pPrint $ parseMaybe statementList ("SELECT person, qwerty FROM qwerty, abc WHERE name <> 'foo';"
                                    ++ "REMOVE FROM qwerty WHERE id > 2")
    pPrint $ parseMaybe statementList ("INSERT INTO qwerty VALUES ('123', 23);"
                                    ++ "UPDATE qwerty SET c1 = v1 * (v2 + v3), c2 = value2 WHERE id > 2 AND NOT id = 5")
    pPrint $ parseMaybe statementList ("SELECT * FROM a JOIN (b JOIN c ON b.fkC = c.pk) ON id > 5 WHERE id > 2")